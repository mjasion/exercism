def latest(scores):
    return scores[-1]


def personal_best(scores):
    bestScore = scores[0]
    for i,e in enumerate(scores):
        if e > bestScore:
            bestScore = e
    return bestScore


def personal_top_three(scores):
    return sorted(scores,reverse=True)[0:3]
